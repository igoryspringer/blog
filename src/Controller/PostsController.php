<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Post;
use App\Form\CommentType;
use App\Form\PostType;
use App\Repository\PostRepository;
use Cocur\Slugify\Slugify;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PostsController.
 *
 * @Route ("/posts")
 */
class PostsController extends AbstractController
{
    /**
     * @var PostRepository
     */
    private $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * @Route("/", name="blog_posts")
     *
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $posts = $this->postRepository->findBy([], ['id' => 'DESC']);

        $pagination = $paginator->paginate(
            $posts,
            $request->query->getInt('page', 1),
            3
        );
        $img = rand(1, 3);

        return $this->render('posts/index.html.twig', [
            'posts' => $pagination,
            'img' => $img,
        ]);
    }

    /**
     * @Route("/new", name="new_blog_post")
     *
     * @param Request $request
     * @param Slugify $slugify
     * @return Response
     */
    public function addPost(Request $request, Slugify $slugify): Response
    {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $post->setSlug($slugify->slugify($post->getTitle()));
            $post->setCreatedAt(new \DateTime());

            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();

            return $this->redirectToRoute('blog_posts');
        }

        return $this->render('posts/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{slug}/edit", name="blog_post_edit")
     *
     * @param Post $post
     * @param Request $request
     * @param Slugify $slugify
     * @return RedirectResponse|Response
     */
    public function edit(Post $post, Request $request, Slugify $slugify)
    {
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $post->setSlug($slugify->slugify($post->getTitle()));

            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();

            return $this->redirectToRoute('blog_posts', [
                'slug' => $post->getSlug(),
            ]);
        }

        return $this->render('posts/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{slug}/delete", name="blog_post_delete")
     *
     * @param Post $post
     * @return RedirectResponse
     */
    public function delete(Post $post)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();

        return $this->redirectToRoute('blog_posts');
    }

    /**
     * @Route("/search", name="blog_search")
     *
     * @param Request $request
     * @return Response
     */
    public function search(Request $request)
    {
        $query = $request->query->get('q');
        $posts = $this->postRepository->searchByQuery($query);

        return $this->render('posts/query_post.html.twig', [
            'posts' => $posts,
        ]);
    }

    /**
     * @Route("/{slug}", methods={"GET", "POST"}, name="blog_show")
     *
     * @param Post $post
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function commentNew(Post $post, Request $request)
    {
        $comment = new Comment();
        $comment->setUsers($this->getUser());
        $post->addComment($comment);

        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        $img = rand(1, 3);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();

            return $this->redirectToRoute('blog_show', ['slug' => $post->getSlug()]);
        }

        return $this->render('posts/show.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
            'img' => $img,
        ]);
    }

    /*
     * @Route("/{slug}/comment/{id}/delete", name="comment_delete", methods={"GET", "DELETE"})
     *
    public function deleteComment(Post $post, Request $request, Comment $comment): Response
    {
        if ($this->isCsrfTokenValid('delete'.$comment->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($comment);
            $entityManager->flush();
        }

        return $this->redirectToRoute('blog_show', ['slug' => $post->getSlug()]);
    }

    /**
     * @Route("{slug}/comment/{id}/edit", name="comment_edit", methods={"GET","POST"})
     *
     * @param Request $request
     * @param Comment $comment
     * @return Response
     *
    public function editComment(Post $post, Request $request, Comment $comment): Response
    {
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('blog_show', ['slug' => $post->getSlug()]);
        }

        return $this->render('posts/show.html.twig', [
            'comment' => $comment,
            'form' => $form->createView(),
        ]);
    }
    */

    /*
     * @Route("/posts/{slug}", name="blog_show")
     *
     * @return Response

    public function post(Post $post)
    {
        return $this->render('posts/1show.html.twig', [
            'post' => $post,
        ]);
    }*/
}

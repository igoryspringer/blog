Installation:
1. $ git clone git@gitlab.com:igoryspringer/blog.git
2. $ composer install --ignore-platform-reqs
3. $ bin/console doctrine:database:create
4. $ bin/console doctrine:schema:update --force
5. $ bin/console doctrine:fixtures:load
6. $ bin/console server:start

Database [PostgreSQL]:
1. $ sudo -u postgres psql postgres
2. $ \password postgres
3. Enter double password for user postgres

Сompleted tasks:

1. Blog (CRUD)
2. Twig
3. ORM Doctrine
4. Fixtures & Faker
5. Slugify
6. Form
7. Webpack Encore
8. Bootstrap
9. Search
10. Check in
11. Registration form with mail
12. Event & EventSubsriber
13. Authorization through API Google & Github
14. Cron